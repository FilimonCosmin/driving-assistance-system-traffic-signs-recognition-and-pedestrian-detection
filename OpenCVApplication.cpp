// OpenCVApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "common.h"
#include "Functions.h"
#include <queue>
#include <iostream>
#include <Windows.h>
#include<stdio.h>
#include <opencv2/video/tracking.hpp>
#include "opencv2/objdetect/objdetect.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/features2d/features2d.hpp"
#include <iostream>

CascadeClassifier trecere_de_pietoni_cascade; 
CascadeClassifier cedeaza_trecerea_cascade; 
CascadeClassifier giratoriu_cascade; 
CascadeClassifier interzis_cascade;
CascadeClassifier oprire_interzisa_cascade;
CascadeClassifier ocolire_stanga_dreapta_cascade;
CascadeClassifier ocolire_stanga_cascade;
CascadeClassifier ocolire_dreapta_cascade;
CascadeClassifier interzis_stanga_cascade;
CascadeClassifier indicator_semafor_cascade;
CascadeClassifier pista_bicicleta_cascade;
CascadeClassifier sens_unic_cascade;
CascadeClassifier inainte_cascade;
CascadeClassifier sapte_cascade;
CascadeClassifier noua_cascade;
CascadeClassifier depasire_interzisa_cascade;
CascadeClassifier semafor_cascade;
CascadeClassifier fullBody_cascade;
CascadeClassifier upperBody_cascade;
CascadeClassifier lowerBody_cascade;

using namespace cv;
using namespace std;

using namespace std;

int zoomRec = 100;
int* percentage;
string Semn_Blue,Semn_Red;

//Functia  de detectie colturi
vector<Point2f> corners(Mat src) {
	
	Mat dst = src.clone();
	int height = src.rows;
	int width = src.cols;

	//Filtru Gaussian
	GaussianBlur(src, src, Size(5, 5), 0.8, 0.8);

	//initilizare variabile functie
	vector<Point2f> corners;
	int maxCorners = 50;
	double qualityLevel = 0.1;
	double minDistance = 15;
	int blockSize = 10;
	bool useHarrisDetector = true;
	double k = 0.04;
	goodFeaturesToTrack(src, corners, maxCorners, qualityLevel, minDistance, Mat(), blockSize, useHarrisDetector, k);
	
	return corners;

}

String oprire_interzisa_cascade_name = "cascade_oprire_interzisa.xml";

String trecere_de_pietoni_cascade_name = "cascade_trecere_de_pietoni.xml";

String giratoriu_cascade_name = "cascade_giratoriu.xml";

String ocolire_stanga_dreapta_cascade_name = "cascade_ocolire_sd.xml";
String ocolire_dreapta_cascade_name = "cascade_ocolire_d.xml";
String ocolire_stanga_cascade_name = "cascade_ocolire_s.xml";

String pista_bicicleta_cascade_name = "cascade_pista_bicicleta.xml";

String sens_unic_cascade_name = "cascade_sens_unic.xml";

String inainte_cascade_name = "cascade_inainte.xml";

Mat Trecere = imread("Indicatoare/Trecere.png", IMREAD_COLOR);
Mat Ocolire = imread("Indicatoare/Ocolire.png", IMREAD_COLOR);
Mat Bicicleta = imread("Indicatoare/Bicicleta.png", IMREAD_COLOR);
Mat Oprire = imread("Indicatoare/Oprire.png", IMREAD_COLOR);
Mat Sens = imread("Indicatoare/Sens Unic.png", IMREAD_COLOR);

//Functia de clasificare semn albastru
string Clasificator_Blue(Mat src) {
	
	String Semn = "";
	int Red_pixels;
	int height = src.rows;
	int width = src.cols;
	Mat HSV, dstBlue, dstYellow, dstRed1, dstRed2, dstRed, dstFinal, dstBlack, dstWhite, dstOrange;

	vector<Point2f> cornersRed, cornersBlue;

	cvtColor(src, HSV, CV_BGR2HSV);
	
	//Initializare numar pixeli 
	Red_pixels = 0;
	//segmentarea culorilor
	inRange(HSV, Scalar(90, 127, 20), Scalar(120, 255, 200), dstBlue);
	inRange(HSV, Scalar(0, 50, 50), Scalar(7, 255, 255), dstRed1);
	inRange(HSV, Scalar(157, 50, 50), Scalar(180, 255, 255), dstRed2);
	dstRed = dstRed1 + dstRed2;
	cornersBlue = corners(dstBlue);
	int number_of_corners = cornersBlue.size();
	//Calcul numar pixeli de culoare
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			if (dstRed.at<uchar>(i, j) == 255) {
				Red_pixels++;
			}
		}
	}
	
	// Incarcare cascades
	if (!trecere_de_pietoni_cascade.load(trecere_de_pietoni_cascade_name))
	{
		printf("Error loading cascade 1!\n");
		
	}
	if (!giratoriu_cascade.load(giratoriu_cascade_name))
	{
		printf("Error loading cascade 2!\n");

	}
	if (!oprire_interzisa_cascade.load(oprire_interzisa_cascade_name))
	{
		printf("Error loading cascade 3!\n");

	}
	if (!ocolire_stanga_dreapta_cascade.load(ocolire_stanga_dreapta_cascade_name))
	{
		printf("Error loading cascade 4!\n");

	}
	if (!ocolire_dreapta_cascade.load(ocolire_dreapta_cascade_name))
	{
		printf("Error loading cascade 5!\n");

	}
	if (!ocolire_stanga_cascade.load(ocolire_stanga_cascade_name))
	{
		printf("Error loading cascade 6!\n");

	}
	if (!pista_bicicleta_cascade.load(pista_bicicleta_cascade_name))
	{
		printf("Error loading cascade 7!\n");

	}
	if (!sens_unic_cascade.load(sens_unic_cascade_name))
	{
		printf("Error loading cascade 8!\n");

	}
	if (!inainte_cascade.load(inainte_cascade_name))
	{
		printf("Error loading cascade 9!\n");

	}
	

	std::vector<Rect> signs_trecere;
	std::vector<Rect> signs_giratoriu;
	std::vector<Rect> signs_oprire_interzisa;
	std::vector<Rect> signs_ocolire_stanga_dreapta;
	std::vector<Rect> signs_ocolire_stanga;
	std::vector<Rect> signs_ocolire_dreapta;
	std::vector<Rect> signs_pista_bicileta;
	std::vector<Rect> signs_sens_unic;
	std::vector<Rect> signs_inainte;
	
	Mat frame_gray;

	cvtColor(src, frame_gray, CV_BGR2GRAY);

	//Detectie machine learning
	trecere_de_pietoni_cascade.detectMultiScale(frame_gray, signs_trecere, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	giratoriu_cascade.detectMultiScale(frame_gray, signs_giratoriu, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	oprire_interzisa_cascade.detectMultiScale(frame_gray, signs_oprire_interzisa, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	ocolire_stanga_dreapta_cascade.detectMultiScale(frame_gray, signs_ocolire_stanga_dreapta, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	ocolire_stanga_cascade.detectMultiScale(frame_gray, signs_ocolire_stanga, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	ocolire_dreapta_cascade.detectMultiScale(frame_gray, signs_ocolire_dreapta, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	pista_bicicleta_cascade.detectMultiScale(frame_gray, signs_pista_bicileta, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	sens_unic_cascade.detectMultiScale(frame_gray, signs_sens_unic, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	inainte_cascade.detectMultiScale(frame_gray, signs_inainte, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	//Clasificare indicator
	if (signs_trecere.size() > 0 && Red_pixels < 10)
	{
		Semn = "Trecere de Pietoni";
		imshow("Trecere de Pietoni", Trecere);
	}
	else if(signs_giratoriu.size() > 0 && Red_pixels < 10)
	{
		Semn = "Sens Giratoriu";
	}
	else if (signs_oprire_interzisa.size() > 0 && Red_pixels >= 15)
	{
		Semn = "Oprire Interzisa";
		imshow("Oprire Interzisa", Oprire);
	}
	else if (signs_pista_bicileta.size() > 0 && Red_pixels < 10)
	{
		Semn = "Pista Bicicleta";
		imshow("Pista Bicicleta", Bicicleta);
	}
	else if ((signs_ocolire_stanga_dreapta.size() > 0 || signs_ocolire_stanga.size() > 0 || signs_ocolire_dreapta.size() > 0) && Red_pixels< 10)
	{
		Semn = "Ocolire";
		imshow("Ocolire", Ocolire);
	}
	else if (signs_sens_unic.size() > 0 && Red_pixels < 10 && number_of_corners < 10)
	{
		Semn = "Sens Unic/Directie Inainte!";
		imshow("Sens Unic/Directie Inainte!", Sens);
	}
	else if (signs_inainte.size() > 0 && Red_pixels < 10 && number_of_corners<10)
	{
		Semn = "Sens Unic\nDirectie Inainte!";
	}
	else
	{
		Semn = "";
	}

	return Semn;
}

String cedeaza_trecerea_name = "cascade_cedeaza_trecerea.xml";

String interzis_name = "cascade_interzis.xml";

String interzis_stanga_cascade_name = "cascade_interzis_stanga.xml";

String indicator_semafor_cascade_name = "cascade_indicator_semafor.xml";

String depasire_interzisa_cascade_name = "cascade_depasire_interzisa.xml";

String noua_cascade_name = "cascade_90_2.xml";

String sapte_cascade_name = "cascade_70.xml";

Mat Limita70 = imread("Indicatoare/70.png", IMREAD_COLOR);
Mat Limita90 = imread("Indicatoare/90.png", IMREAD_COLOR);
Mat Depasire = imread("Indicatoare/Depasire.png", IMREAD_COLOR);
Mat Interzis_Stanga = imread("Indicatoare/Interzis Stanga.png", IMREAD_COLOR);
Mat Semafoare = imread("Indicatoare/Semafoare.png", IMREAD_COLOR);

//Functia de clasificare semn rosu
string Clasificator_Red(Mat src) {
	
	String Semn = "";
	
	int h[4], percentage[6];
	int height = src.rows;
	int width = src.cols;
	Mat HSV, dstBlue, dstYellow, dstRed1, dstRed2, dstRed, dstFinal, dstBlack, dstWhite, dstOrange;

	vector<Point2f> cornersRed;

	cvtColor(src, HSV, CV_BGR2HSV);
	
	//Initializare procentaje de culoare
	for (int i = 0; i < 4; i++) {
		h[i] = 0;
	}
	inRange(HSV, Scalar(90, 127, 20), Scalar(100, 255, 200), dstBlue);
	inRange(HSV, Scalar(0, 0, 0), Scalar(255, 200, 100), dstBlack);
	inRange(HSV, Scalar(0, 0, 150), Scalar(255, 50, 255), dstWhite);
	inRange(HSV, Scalar(0, 100, 50), Scalar(10, 255, 255), dstRed1);
	inRange(HSV, Scalar(157, 100, 50), Scalar(180, 255, 255), dstRed2);
	dstRed = dstRed1 + dstRed2;
	cornersRed = corners(dstRed);
	int number_of_corners = cornersRed.size();
	//Calcul numar de pixeli din fiecare culoare
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			if (dstBlue.at<uchar>(i, j) == 255) {
				h[0]++;
			}
			if (dstBlack.at<uchar>(i, j) == 255) {
				h[1]++;
			}
			if (dstWhite.at<uchar>(i, j) == 255) {
				h[2]++;
			}
		}
	}
	
	// Incarcare cascades
	if (!cedeaza_trecerea_cascade.load(cedeaza_trecerea_name))
	{
		printf("Error loading cascade !\n");
		
	}
	if (!interzis_cascade.load(interzis_name))
	{
		printf("Error loading cascade !\n");

	}
	if (!interzis_stanga_cascade.load(interzis_stanga_cascade_name))
	{
		printf("Error loading cascade !\n");

	}
	if (!indicator_semafor_cascade.load(indicator_semafor_cascade_name))
	{
		printf("Error loading cascade !\n");

	}
	if (!depasire_interzisa_cascade.load(depasire_interzisa_cascade_name))
	{
		printf("Error loading cascade !\n");

	}
	if (!noua_cascade.load(noua_cascade_name))
	{
		printf("Error loading cascade !\n");
	}
	if (!sapte_cascade.load(sapte_cascade_name))
	{
		printf("Error loading cascade !\n");

	}

	std::vector<Rect> signs_cedeaza;
	std::vector<Rect> signs_interzis;
	std::vector<Rect> signs_interzis_stanga;
	std::vector<Rect> signs_indicator_semafor;
	std::vector<Rect> signs_noua;
	std::vector<Rect> signs_sapte;
	std::vector<Rect> signs_depasire_interzisa;

	Mat frame_gray;

	cvtColor(src, frame_gray, CV_BGR2GRAY);

	//Detectie machine learning
	cedeaza_trecerea_cascade.detectMultiScale(frame_gray, signs_cedeaza, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	interzis_cascade.detectMultiScale(frame_gray, signs_interzis, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	interzis_stanga_cascade.detectMultiScale(frame_gray, signs_interzis_stanga, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	indicator_semafor_cascade.detectMultiScale(frame_gray, signs_indicator_semafor, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	depasire_interzisa_cascade.detectMultiScale(frame_gray, signs_depasire_interzisa, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	noua_cascade.detectMultiScale(frame_gray, signs_noua, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	sapte_cascade.detectMultiScale(frame_gray, signs_sapte, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

	//Clasificare indicator
	if (signs_cedeaza.size() > 0 && h[2]> 10 && h[0]<10) {
		Semn = "Cedeaza Trecerea!";
	}
	else if(signs_interzis.size()>0 && h[2] > 10 && h[0] < 10)
	{
		Semn = "Interzis!";
	}
	else if (signs_interzis_stanga.size() > 0 && h[2] > 10 && h[1]>20 && h[0] < 10)
	{
		Semn = "Interzis virajul la stanga!";
		imshow("Interzis virajul la stanga!", Interzis_Stanga);
	}
	else if (signs_indicator_semafor.size() > 0 && h[2] > 10 && h[0] < 10)
	{
		Semn = "Urmeaza Semafor!";
		imshow("Urmeaza Semafor!", Semafoare);
	}
	else if (signs_depasire_interzisa.size() > 0)
	{
		Semn = "Depasire interzisa!";
		imshow("Depasire interzisa!", Depasire);
	}
	else if (signs_sapte.size() > 0 && h[2] > 500 && h[2] < 1000)
	{
		Semn = "70";
		imshow("Limita de Viteza", Limita70);
	}
	else if (signs_noua.size() > 0 && h[0] < 50 && h[2] > 600 && h[2] < 1000)
	{
		Semn = "90";
		imshow("Limita de Viteza", Limita90 );
	}
	else
	{
		Semn = "";
	}
	
	return Semn;
}

//Functia de extragere ROI
vector<Rect> BoundingBox(Mat Src) {
	
	int max_height = 0;
	int max_width = 0;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	findContours(Src, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
	vector<vector<Point> > contours_poly(contours.size());
	vector<Rect> boundRect(contours.size());
	vector<Rect> boundRect_Final(contours.size());
	vector<Point2f>center(contours.size());
	vector<float>radius(contours.size());
	int j = 0;
	for (int i = 0; i < contours.size(); i++)
	{
		approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
		boundRect[i] = boundingRect(Mat(contours_poly[i]));
		float AspectRatio= 0;
		//Calcul ApectRatio
		if (boundRect[i].width > 0 && boundRect[i].height){
			AspectRatio = (float)boundRect[i].height / (float)boundRect[i].width;
		}
		//Verificare forma indicator
		if (AspectRatio > 0.75 && AspectRatio < 1.25 && boundRect[i].height > 35 && boundRect[i].height < 150) {
			boundRect_Final[j] = boundRect[i];
			j++;
		}
		minEnclosingCircle(contours_poly[i], center[i], radius[i]);
	}

	return boundRect_Final;
}

//Functie de detectie pietoni
Mat PersonsDetection(Mat src) {
	
	Mat dst;
	dst = src;
	String fullBody_cascade_name = "haarcascade_fullbody.xml";
	if (!fullBody_cascade.load(fullBody_cascade_name))
	{
		printf("Error loading face cascades !\n");
	}

	std::vector<Rect> full_Body;

	Mat frame_gray;
	cvtColor(src, frame_gray, CV_BGR2GRAY);
	equalizeHist(frame_gray, frame_gray);
	int person_Height = 100;

	fullBody_cascade.detectMultiScale(frame_gray, full_Body, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(0.4 * person_Height, person_Height));

	for (int i = 0; i < full_Body.size(); i++)
	{
		rectangle(dst, full_Body[i], Scalar(255, 255, 0), 1, 8, 0);
		printf("%s\n","Atentie pieton!");
	}
	
	return dst;
}

int Orange,Green,Red;
int m, n;

//Functia principala
void VideoDisplay1() {

	Mat HSV, dstBlue, dstOrange, dstGreen, dstRed1, dstRed2, dstRed, dstBlack, ROI_Blue, ROI_Red, edges;
	vector<Rect> boundRect_Blue;
	vector<Rect> boundRect_Red;
	vector<Rect> boundRect_Semafor;
	Mat element2 = getStructuringElement(MORPH_RECT, Size(3, 3));
	Mat src;
	int i;
	int count = 0;
	int count2 = 0;
	String limita = "necunoscuta";

	// 0 dechide webcam-ul, altfel dam ca argument calea catre un video
	VideoCapture video("Input/Video2.mp4");
	// Verificam daca videoclipul este deschis
	if (!video.isOpened()) {
		printf("Cannot open video capture device.\n");
		waitKey();
		return;
	}
	// Pentru salvarea cadrului
	cv::Mat frame;
	// Salvam rezolutia
	int frameWidth = video.get(cv::CAP_PROP_FRAME_WIDTH);
	int frameHeigth = video.get(cv::CAP_PROP_FRAME_HEIGHT);
	// Obiect de scriere a videoclipului
	cv::VideoWriter output("Output/Video2_Output.avi",
							cv::VideoWriter::fourcc('M','J','P','G'),
							30,
							cv::Size(frameWidth, frameHeigth));
	// Bucla cu toate cadrele
	while (video.read(frame)) {

		
		src = frame;
		Mat dst = src.clone();
		//filtru Gaussian 
		GaussianBlur(src, src, Size(5, 5), 0.8, 0.8);
		//RGB - HSV
		cvtColor(src, HSV, CV_BGR2HSV);
		//Mastile pentru culoare
		
		inRange(HSV, Scalar(90, 127, 20), Scalar(120, 255, 200), dstBlue);
		inRange(HSV, Scalar(0, 100, 50), Scalar(10, 255, 255), dstRed1);
		inRange(HSV, Scalar(157, 100, 50), Scalar(180, 255, 255), dstRed2);
		dstRed = dstRed1 + dstRed2;
		inRange(HSV, Scalar(10, 100, 20), Scalar(25, 255, 255), dstOrange);
		inRange(HSV, Scalar(25, 25, 25), Scalar(70, 255, 255), dstGreen);
		
		//Eroziuni/Dilatari
		erode(dstBlue, dstBlue, element2, Point(-1, -1), 2);
		dilate(dstBlue, dstBlue, element2, Point(-1, -1), 5);
		erode(dstRed, dstRed, element2, Point(-1, -1), 1);
		dilate(dstRed, dstRed, element2, Point(-1, -1), 3);
		erode(dstRed, dstRed, element2, Point(-1, -1), 1);
		
		//Extragem ROI
		boundRect_Blue = BoundingBox(dstBlue);
		boundRect_Red = BoundingBox(dstRed);
		
		//Detectie semafor
		String semafor_cascade_name = "cascade_semafor.xml";

		if (!semafor_cascade.load(semafor_cascade_name))
		{
			printf("Error loading cascade !\n");
		}
		std::vector<Rect> semafor;

		Mat frame_gray;

		cvtColor(src, frame_gray, CV_BGR2GRAY);

		semafor_cascade.detectMultiScale(frame_gray, semafor, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));
		
		for (int i = 0; i < semafor.size(); i++)
		{
			Orange = 0;
			Green = 0;
			Red = 0;
			//Determinare culoare semafor
			if (semafor[i].y + semafor[i].width < frame.cols && semafor[i].x + semafor[i].height < frame.rows) {
				for (m = semafor[i].y; m < semafor[i].y + semafor[i].width; m++) {
					for (n = semafor[i].x; n < semafor[i].x + semafor[i].height; n++) {
						if (dstOrange.at<uchar>(m, n) == 255) {
							Orange++;
						}
						if (dstGreen.at<uchar>(m, n) == 255) {
							Green++;
						}
						if (dstRed.at<uchar>(m, n) == 255) {
							Red++;
						}
					}
				}
			}
			//Incadrare semafor si afisare avertisment
			
			if (Green > 15) {
				printf("%s\n", "Semafor Verde!");
				rectangle(dst, semafor[i], Scalar(0, 255, 0), 1, 8, 0);
				rectangle(frame, semafor[i], Scalar(0, 255, 0), 1, 8, 0);
			}
			else if (Orange > 15) {
				printf("%s\n", "Atentie Semafor Portocaliu!");
				rectangle(dst, semafor[i], Scalar(0, 140, 255), 1, 8, 0);
				rectangle(frame, semafor[i], Scalar(0, 140, 255), 1, 8, 0);
			}
			else if (Red > 15) {
				printf("%s\n", "Atentie Semafor Rosu!");
				rectangle(dst, semafor[i], Scalar(0, 0, 255), 1, 8, 0);
				rectangle(frame, semafor[i], Scalar(0, 0, 255), 1, 8, 0);
			}
			
		}

		//Detectie semn albastru
		for (i = 0; i < boundRect_Blue.size(); i++) {
			ROI_Blue = src(boundRect_Blue[i]);
			if (!ROI_Blue.empty()) {
				Point stanga_sus_Blue(boundRect_Blue[i].x, boundRect_Blue[i].y);
				Point dreapta_jos_Blue(boundRect_Blue[i].x + boundRect_Blue[i].height, 
										boundRect_Blue[i].y + boundRect_Blue[i].width);
				Point Text_Blue(boundRect_Blue[i].x, boundRect_Blue[i].y + boundRect_Blue[i].width + 10);
				//Functia de detectie semn albastru
				Semn_Blue = Clasificator_Blue(ROI_Blue);
				
				if (Semn_Blue != "") {

					if (Semn_Blue == "Trecere de Pietoni") {

						//Daca intalnim trecere de pietoni incepem detectia de persoane
						dst = PersonsDetection(dst);
						frame = dst;
						//Incadrare semn si afisare denumire
						printf("%s\n", Semn_Blue.c_str());
						rectangle(dst, stanga_sus_Blue, dreapta_jos_Blue, 1, 2, 0);
					
						cv::putText(dst,
							Semn_Blue.c_str(),
							Text_Blue, // Coordinates
							cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
							1, // Scale. 2.0 = 2x bigger
							cv::Scalar(0, 0, 255), // BGR Color
							1); // Line Thickness (Optiona
						
						rectangle(frame, stanga_sus_Blue, dreapta_jos_Blue, 1, 2, 0);

						cv::putText(frame,
							Semn_Blue.c_str(),
							Text_Blue, // Coordinates
							cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
							1, // Scale. 2.0 = 2x bigger
							cv::Scalar(0, 0, 255), // BGR Color
							1); // Line Thickness (Optiona
						
					}
					else{
						//Incadrare semn si afisare denumire
						printf("%s\n", Semn_Blue.c_str());
						rectangle(dst, stanga_sus_Blue, dreapta_jos_Blue, 1, 2, 0);

						cv::putText(dst,
							Semn_Blue.c_str(),
							Text_Blue, // Coordinates
							cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
							1, // Scale. 2.0 = 2x bigger
							cv::Scalar(0, 0, 255), // BGR Color
							1); // Line Thickness (Optiona

						rectangle(frame, stanga_sus_Blue, dreapta_jos_Blue, 1, 2, 0);

						cv::putText(frame,
							Semn_Blue.c_str(),
							Text_Blue, // Coordinates
							cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
							1, // Scale. 2.0 = 2x bigger
							cv::Scalar(0, 0, 255), // BGR Color
							1); // Line Thickness (Optiona
					
					}
				}
			}
		}
		//Detectie semn Rosu
		for (i = 0; i < boundRect_Red.size(); i++) {
			ROI_Red = src(boundRect_Red[i]);
			if (!ROI_Red.empty()) {
				Point stanga_sus_Red(boundRect_Red[i].x, boundRect_Red[i].y);
				Point dreapta_jos_Red(boundRect_Red[i].x + boundRect_Red[i].height + 15,
										boundRect_Red[i].y + boundRect_Red[i].width);
				Point Text_Red(boundRect_Red[i].x, boundRect_Red[i].y + boundRect_Red[i].width + 10);
				//Functia de detectie semn rosu
				Semn_Red = Clasificator_Red(ROI_Red);
				//Restrictie de viteza
				if (Semn_Red != "") {
					printf("%s\n", Semn_Red.c_str());
					if (Semn_Red == "90") {
						limita = "90";
					}
					else if (Semn_Red == "70") {
						limita = "70";
					}
					else
					{
						//Incadrare semn si afisare denumire
						rectangle(dst, stanga_sus_Red, dreapta_jos_Red, 1, 2, 0);

						cv::putText(dst,
							Semn_Red.c_str(),
							Text_Red, // Coordinates
							cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
							1, // Scale. 2.0 = 2x bigger
							cv::Scalar(0, 0, 255), // BGR Color
							1); // Line Thickness (Optiona

						rectangle(frame, stanga_sus_Red, dreapta_jos_Red, 1, 2, 0);

						cv::putText(frame,
							Semn_Red.c_str(),
							Text_Red, // Coordinates
							cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
							1, // Scale. 2.0 = 2x bigger
							cv::Scalar(0, 0, 255), // BGR Color
							1); // Line Thickness (Optiona
						
					}
				}
			}

		}
		//Afisare limita de viteza 
		cv::putText(dst,
			"Limita de viteza "+limita,
			Point(0,15), // Coordinates
			cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
			1, // Scale. 2.0 = 2x bigger
			cv::Scalar(0, 0, 255), // BGR Color
			1); // Line Thickness (Optiona

		cv::putText(frame,
			"Limita de viteza " + limita,
			Point(0,15), // Coordinates
			cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
			1, // Scale. 2.0 = 2x bigger
			cv::Scalar(0, 0, 255), // BGR Color
			1); // Line Thickness (Optiona
		cv::imshow("Video feed", frame);
		// Scrierea cadrului video 
		output.write(dst);
		// Pentru intreruperea buclei
		if (cv::waitKey(25) >= 0) break;

	} // end while (video.read(frame))

	// Eliberare video capture and writer
	output.release();
	video.release();

	cv::destroyAllWindows();
}

void VideoDisplay2() {

	Mat HSV, dstBlue, dstOrange, dstGreen, dstRed1, dstRed2, dstRed, dstBlack, ROI_Blue, ROI_Red, edges;
	vector<Rect> boundRect_Blue;
	vector<Rect> boundRect_Red;
	vector<Rect> boundRect_Semafor;
	Mat element2 = getStructuringElement(MORPH_RECT, Size(3, 3));
	Mat src;
	int i;
	int count = 0;
	int count2 = 0;
	String limita = "necunoscuta";

	// 0 dechide webcam-ul, altfel dam ca argument calea catre un video
	VideoCapture video("Input/Video1.mp4");
	// Verificam daca videoclipul este deschis
	if (!video.isOpened()) {
		printf("Cannot open video capture device.\n");
		waitKey();
		return;
	}
	// Pentru salvarea cadrului
	cv::Mat frame;
	// Salvam rezolutia
	int frameWidth = video.get(cv::CAP_PROP_FRAME_WIDTH);
	int frameHeigth = video.get(cv::CAP_PROP_FRAME_HEIGHT);
	// Obiect de scriere a videoclipului
	cv::VideoWriter output("Output/Video1_Output.avi",
		cv::VideoWriter::fourcc('M', 'J', 'P', 'G'),
		30,
		cv::Size(frameWidth, frameHeigth));
	// Bucla cu toate cadrele
	while (video.read(frame)) {


		src = frame;
		Mat dst = src.clone();
		//filtru Gaussian 
		GaussianBlur(src, src, Size(5, 5), 0.8, 0.8);
		//RGB - HSV
		cvtColor(src, HSV, CV_BGR2HSV);
		//Mastile pentru culoare

		inRange(HSV, Scalar(90, 127, 20), Scalar(120, 255, 200), dstBlue);
		inRange(HSV, Scalar(0, 100, 50), Scalar(10, 255, 255), dstRed1);
		inRange(HSV, Scalar(157, 100, 50), Scalar(180, 255, 255), dstRed2);
		dstRed = dstRed1 + dstRed2;
		inRange(HSV, Scalar(10, 100, 20), Scalar(25, 255, 255), dstOrange);
		inRange(HSV, Scalar(25, 25, 25), Scalar(70, 255, 255), dstGreen);

		//Eroziuni/Dilatari
		erode(dstBlue, dstBlue, element2, Point(-1, -1), 2);
		dilate(dstBlue, dstBlue, element2, Point(-1, -1), 5);
		erode(dstRed, dstRed, element2, Point(-1, -1), 1);
		dilate(dstRed, dstRed, element2, Point(-1, -1), 3);
		erode(dstRed, dstRed, element2, Point(-1, -1), 1);

		//Extragem ROI
		boundRect_Blue = BoundingBox(dstBlue);
		boundRect_Red = BoundingBox(dstRed);

		//Detectie semafor
		String semafor_cascade_name = "cascade_semafor.xml";

		if (!semafor_cascade.load(semafor_cascade_name))
		{
			printf("Error loading cascade !\n");
		}
		std::vector<Rect> semafor;

		Mat frame_gray;

		cvtColor(src, frame_gray, CV_BGR2GRAY);

		semafor_cascade.detectMultiScale(frame_gray, semafor, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(10, 10));

		for (int i = 0; i < semafor.size(); i++)
		{
			Orange = 0;
			Green = 0;
			Red = 0;
			//Determinare culoare semafor
			if (semafor[i].y + semafor[i].width < frame.cols && semafor[i].x + semafor[i].height < frame.rows) {
				for (m = semafor[i].y; m < semafor[i].y + semafor[i].width; m++) {
					for (n = semafor[i].x; n < semafor[i].x + semafor[i].height; n++) {
						if (dstOrange.at<uchar>(m, n) == 255) {
							Orange++;
						}
						if (dstGreen.at<uchar>(m, n) == 255) {
							Green++;
						}
						if (dstRed.at<uchar>(m, n) == 255) {
							Red++;
						}
					}
				}
			}
			//Incadrare semafor si afisare avertisment

			if (Green > 15) {
				printf("%s\n", "Semafor Verde!");
				rectangle(dst, semafor[i], Scalar(0, 255, 0), 1, 8, 0);
				rectangle(frame, semafor[i], Scalar(0, 255, 0), 1, 8, 0);
			}
			else if (Orange > 15) {
				//printf("%s\n", "Atentie Semafor Portocaliu!");
				rectangle(dst, semafor[i], Scalar(0, 140, 255), 1, 8, 0);
				rectangle(frame, semafor[i], Scalar(0, 140, 255), 1, 8, 0);
			}
			else if (Red > 15) {
				printf("%s\n", "Atentie Semafor Rosu!");
				rectangle(dst, semafor[i], Scalar(0, 0, 255), 1, 8, 0);
				rectangle(frame, semafor[i], Scalar(0, 0, 255), 1, 8, 0);
			}

		}

		//Detectie semn albastru
		for (i = 0; i < boundRect_Blue.size(); i++) {
			ROI_Blue = src(boundRect_Blue[i]);
			if (!ROI_Blue.empty()) {
				Point stanga_sus_Blue(boundRect_Blue[i].x, boundRect_Blue[i].y);
				Point dreapta_jos_Blue(boundRect_Blue[i].x + boundRect_Blue[i].height,
					boundRect_Blue[i].y + boundRect_Blue[i].width);
				Point Text_Blue(boundRect_Blue[i].x, boundRect_Blue[i].y + boundRect_Blue[i].width + 10);
				//Functia de detectie semn albastru
				Semn_Blue = Clasificator_Blue(ROI_Blue);

				if (Semn_Blue != "") {

					if (Semn_Blue == "Trecere de Pietoni") {

						//Daca intalnim trecere de pietoni incepem detectia de persoane
						dst = PersonsDetection(dst);
						frame = dst;
						//Incadrare semn si afisare denumire
						printf("%s\n", Semn_Blue.c_str());
						rectangle(dst, stanga_sus_Blue, dreapta_jos_Blue, 1, 2, 0);

						cv::putText(dst,
							Semn_Blue.c_str(),
							Text_Blue, // Coordinates
							cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
							1, // Scale. 2.0 = 2x bigger
							cv::Scalar(0, 0, 255), // BGR Color
							1); // Line Thickness (Optiona

						rectangle(frame, stanga_sus_Blue, dreapta_jos_Blue, 1, 2, 0);

						cv::putText(frame,
							Semn_Blue.c_str(),
							Text_Blue, // Coordinates
							cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
							1, // Scale. 2.0 = 2x bigger
							cv::Scalar(0, 0, 255), // BGR Color
							1); // Line Thickness (Optiona

					}
					else {
						//Incadrare semn si afisare denumire
						printf("%s\n", Semn_Blue.c_str());
						rectangle(dst, stanga_sus_Blue, dreapta_jos_Blue, 1, 2, 0);

						cv::putText(dst,
							Semn_Blue.c_str(),
							Text_Blue, // Coordinates
							cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
							1, // Scale. 2.0 = 2x bigger
							cv::Scalar(0, 0, 255), // BGR Color
							1); // Line Thickness (Optiona

						rectangle(frame, stanga_sus_Blue, dreapta_jos_Blue, 1, 2, 0);

						cv::putText(frame,
							Semn_Blue.c_str(),
							Text_Blue, // Coordinates
							cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
							1, // Scale. 2.0 = 2x bigger
							cv::Scalar(0, 0, 255), // BGR Color
							1); // Line Thickness (Optiona

					}
				}
			}
		}
		//Detectie semn Rosu
		for (i = 0; i < boundRect_Red.size(); i++) {
			ROI_Red = src(boundRect_Red[i]);
			if (!ROI_Red.empty()) {
				Point stanga_sus_Red(boundRect_Red[i].x, boundRect_Red[i].y);
				Point dreapta_jos_Red(boundRect_Red[i].x + boundRect_Red[i].height + 15,
					boundRect_Red[i].y + boundRect_Red[i].width);
				Point Text_Red(boundRect_Red[i].x, boundRect_Red[i].y + boundRect_Red[i].width + 10);
				//Functia de detectie semn rosu
				Semn_Red = Clasificator_Red(ROI_Red);
				//Restrictie de viteza
				if (Semn_Red != "") {
					printf("%s\n", Semn_Red.c_str());
					if (Semn_Red == "90") {
						limita = "90";
					}
					else if (Semn_Red == "70") {
						limita = "70";
					}
					else
					{
						//Incadrare semn si afisare denumire
						rectangle(dst, stanga_sus_Red, dreapta_jos_Red, 1, 2, 0);

						cv::putText(dst,
							Semn_Red.c_str(),
							Text_Red, // Coordinates
							cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
							1, // Scale. 2.0 = 2x bigger
							cv::Scalar(0, 0, 255), // BGR Color
							1); // Line Thickness (Optiona

						rectangle(frame, stanga_sus_Red, dreapta_jos_Red, 1, 2, 0);

						cv::putText(frame,
							Semn_Red.c_str(),
							Text_Red, // Coordinates
							cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
							1, // Scale. 2.0 = 2x bigger
							cv::Scalar(0, 0, 255), // BGR Color
							1); // Line Thickness (Optiona

					}
				}
			}

		}
		//Afisare limita de viteza 
		cv::putText(dst,
			"Limita de viteza " + limita,
			Point(0, 15), // Coordinates
			cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
			1, // Scale. 2.0 = 2x bigger
			cv::Scalar(0, 0, 255), // BGR Color
			1); // Line Thickness (Optiona

		cv::putText(frame,
			"Limita de viteza " + limita,
			Point(0, 15), // Coordinates
			cv::FONT_HERSHEY_COMPLEX_SMALL, // Font
			1, // Scale. 2.0 = 2x bigger
			cv::Scalar(0, 0, 255), // BGR Color
			1); // Line Thickness (Optiona
		cv::imshow("Video feed", frame);
		// Scrierea cadrului video 
		output.write(dst);
		// Pentru intreruperea buclei
		if (cv::waitKey(25) >= 0) break;

	} // end while (video.read(frame))

	// Eliberare video capture and writer
	output.release();
	video.release();

	cv::destroyAllWindows();
}

int main()
{
	int op;
	do
	{
		system("cls");
		destroyAllWindows();
		printf("Menu:\n");
		printf(" 1 - VideoDisplay1\n");
		printf(" 2 - VideoDisplay2\n");
		printf(" 0 - Exit\n\n");
		printf("Option: ");
		scanf("%d",&op);
		switch (op)
		{
			case 1:
				VideoDisplay1();
				break;
			case 2:
				VideoDisplay2();
				break;

		}
	}
	while (op!=0);
	return 0;
}